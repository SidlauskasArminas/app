package god.knows.myapplication2;

import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SecondDAO {
    @Insert
    void insert(Second second);

    @Query("DELETE FROM Second")
    void deleteAll();

    @Query("SELECT * from second ORDER BY second ASC")
    List<Second> getAllSeconds();
}
