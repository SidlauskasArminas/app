package god.knows.myapplication2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AppDatabase mDb;
    private TextView txt_list;
    private Button button;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Person a = new Person();
        a.setName("Kaka");
        a.setPhoneNumber("8888");
        a.setSurname("Makaka");
        mDb = AppDatabase.getInstance(this);
        mDb.personDAO().insert(a);

        txt_list = (TextView) findViewById(R.id.txt_list);

        etFirstName = (EditText) findViewById(R.id.edittext_name);
        etLastName = (EditText) findViewById(R.id.edittext_surname);
        etPhoneNumber = (EditText) findViewById(R.id.edittext_phone);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etFirstName.getText().toString().trim();
                String surname = etLastName.getText().toString().trim();
                String phoneNumber = etPhoneNumber.getText().toString().trim();

                if(TextUtils.isEmpty(name) || TextUtils.isEmpty(surname)
                        || TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(getApplicationContext(),
                            "Name/Surname/Phone Number should not be empty",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Person person = new Person();
                    person.setName(name);
                    person.setSurname(surname);
                    person.setPhoneNumber(phoneNumber);
                    mDb.personDAO().insert(person);
                    Toast.makeText(getApplicationContext(), "Svaved successfully", Toast.LENGTH_SHORT).show();
                    etFirstName.setText("");
                    etLastName.setText("");
                    etPhoneNumber.setText("");
                    etFirstName.requestFocus();
                    getPersonList();
                }
            }
        });
        getPersonList();
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_save, 0, 0, 0);
    }

    private void getPersonList() {
        txt_list.setText("");
        List<Person> personList = mDb.personDAO().getAllPersons();
        for(Person person : personList) {
            txt_list.append(String.format("%-20s",person.getName()) + " " +
                    person.getSurname() + " : " + person.getPhoneNumber());
            txt_list.append("\n");
        }
    }
}
