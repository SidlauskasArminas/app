package god.knows.myapplication2;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Second")
public class Second {
        @PrimaryKey(autoGenerate = true)
        private long id;

        @NonNull
        @ColumnInfo(name = "second")
        private String second;

        @NonNull
        public void setId(@NonNull long id) {
            this.id = id;
        }

        @NonNull
        public long getId() {
            return this.id;
        }

        public void setSecond(@NonNull String second) {
            this.second = second;
        }

        @NonNull
        public String getSecond() {
            return this.second = second;
        }
}
