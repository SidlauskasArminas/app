package god.knows.popupapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Dialog myDialog;
    Button buttonTime;
    Button buttonLoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDialog = new Dialog(this);

    }

    public void ShowPopup(View v){
        TextView txtclose;
        myDialog.setContentView(R.layout.custompopup);
        txtclose = (TextView) myDialog.findViewById(R.id.textView_close_popup);
        buttonTime = (Button) myDialog.findViewById(R.id.button_select_time_notification);
        buttonLoc = (Button) myDialog.findViewById(R.id.button_select_location_notification);

        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        buttonLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLoc.setBackgroundResource(R.drawable.roundbuttonstyleclick);
                try {
                    wait(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                myDialog.dismiss();
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        myDialog.show();
    }
}
