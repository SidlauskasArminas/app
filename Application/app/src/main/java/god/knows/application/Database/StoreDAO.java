package god.knows.application.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StoreDAO {
    @Insert
    void insert(Store store);

    @Query("DELETE FROM Store")
    void deleteAll();

    @Query("DELETE FROM Store WHERE id = :number")
    void delete(long number);

    @Query("SELECT * from Store ORDER BY name ASC")
    List<Store> getAllStores();

    @Query("SELECT COUNT(*) from Store WHERE name = :word")
    int checkIfExists(String word);

    @Query("SELECT id FROM Store WHERE name = :word")
    int getStoreIdByName(String word);
}
