package god.knows.application.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Product;
import god.knows.application.Database.Suggestions;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;
import god.knows.application.SuggestionsAdapter;

public class AddProduct extends DrawerMain {

    private AppDatabase mDb;
    private Button button;
    private EditText etDescription;
    private long StoreId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_add_product, null, false);
        drawerLayout.addView(contentView, 0);

        StoreId = getIntent().getLongExtra("StoreId", 0);
        mDb = AppActivity.getDatabase();

        etDescription = (EditText) findViewById(R.id.editProduct);

        String[] products = getSuggestionList();
        SuggestionsAdapter adapter = new SuggestionsAdapter(this,
                android.R.layout.simple_dropdown_item_1line, products);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.editProduct);

        //textView.setDropDownHeight(280);
        textView.setDropDownAnchor(R.id.editProduct);
        textView.setThreshold(1);
        textView.setAdapter(adapter);

        button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = etDescription.getText().toString().trim();
                
                if(TextUtils.isEmpty(description)) {
                    Toast.makeText(getApplicationContext(),
                            "Product should not be empty",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Product product = new Product();
                    product.setDescription(description);
                    product.setStoreId(StoreId);
                    Suggestions suggestion = new Suggestions();
                    suggestion.setWord(description);
                    suggestion.setStoreId(StoreId);
                    //Gets current date and time in a string format
                    Date date1 = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    product.setDate(dateFormat.format(date1));

                    Log.i("Pries ikeliant i db", "ivykdyta");
                    mDb.productDAO().insert(product);
                    int amount = mDb.suggestionsDAO().checkIfExists(product.getDescription());
                    if(amount == 0)
                        mDb.suggestionsDAO().insert(suggestion);
                    Log.i("Po ikelimo i db", "ivykdyta");
                    Toast.makeText(getApplicationContext(), "Saved successfully", Toast.LENGTH_SHORT).show();
                    etDescription.setText("");
                    etDescription.requestFocus();
                    finish();
                    startActivity(getIntent());
                    //getSuggestionList();
                }
            }
        });
        //getSuggestionList();
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_save, 0, 0, 0);
    }


    private String[] getSuggestionList() {
        final List<Suggestions> SuggestionList = mDb.suggestionsDAO().getAllSuggestions(StoreId);
        final String suggestions[] = new String[SuggestionList.size()];
        int i = 0;
        for(Suggestions Suggestion : SuggestionList) {
            suggestions[i++] = Suggestion.getWord().trim();
        }
        return suggestions;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ProductSelection.class);
        intent.putExtra("StoreId", StoreId);
        startActivity(intent);
        super.onBackPressed();
    }
}
