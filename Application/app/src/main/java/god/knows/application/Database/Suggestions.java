package god.knows.application.Database;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Suggestions {
    @NonNull
    @PrimaryKey(autoGenerate =  true)
    @ColumnInfo(name = "id")
    private long id;

    @NonNull
    @ColumnInfo(name = "word")
    private String word;

    @NonNull
    @ColumnInfo(name = "StoreId")
    private long StoreId;

    @NonNull
    public void setId(@NonNull long id){
        this.id = id;
    }

    @NonNull
    public long getId(){ return this.id; }

    @NonNull
    public void setWord(@NonNull String word) { this.word = word; }

    @NonNull
    public String getWord() { return word; }

    @NonNull
    public void setStoreId(@NonNull long storeId) { StoreId = storeId; }

    @NonNull
    public long getStoreId() { return StoreId; }
}
