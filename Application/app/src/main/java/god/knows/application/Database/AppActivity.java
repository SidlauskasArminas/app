package god.knows.application.Database;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;


public class AppActivity extends Application {
    static AppDatabase db;

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `Suggestions` (`id` INTEGER, "
                    + "`word` TEXT, PRIMARY KEY(`id`))");
        }
    };

    @Override
    public void onCreate(){
        super.onCreate();
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "App_database8")
                .allowMainThreadQueries()
                .build();
    }

    public static  AppDatabase getDatabase(){
        return db;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
