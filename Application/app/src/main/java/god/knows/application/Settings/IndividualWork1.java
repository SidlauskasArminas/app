package god.knows.application.Settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import god.knows.application.R;

public class IndividualWork1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_work1);

        String word = (String) getIntent().getSerializableExtra("Data");
        TextView textView = findViewById(R.id.textView_settings_ind2_fill);
        textView.setText(word);
        textView.setVisibility(View.VISIBLE);
    }
}
