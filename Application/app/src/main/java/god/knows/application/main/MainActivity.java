package god.knows.application.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import god.knows.application.Database.AppDatabase;
import god.knows.application.Drawer.DrawView;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;
import god.knows.application.Settings.Settings;
import god.knows.application.main.AddStore;
import god.knows.application.main.StoreSelection;

public class MainActivity extends DrawerMain {
    private static final String FILE_NAME = "example.txt";

    private DrawView drawView;
    private AppDatabase mDb;
    EditText mEditText;
    Button _button;
    Button _button3;
    Button _buttonExit;

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_main, null, false);
        drawerLayout.addView(contentView, 0);

        drawView = (DrawView) findViewById(R.id.drawView);
        //mDb = AppActivity.getDatabase();
        //Our button
        _button = (Button) findViewById(R.id.button);
        _button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //onClick
                Intent intent = new Intent(getBaseContext(), StoreSelection.class);
                //intent.putExtra("Data", "Hello World");
                startActivity(intent);
            }
        });

        _button3 = (Button) findViewById(R.id.button3);
        _button3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //onClick
                Intent intent1 = new Intent(getBaseContext(), Settings.class);
                //intent.putExtra("Data", "Hello World");
                startActivity(intent1);
            }
        });

        _buttonExit = (Button) findViewById(R.id.button_main_exit);
        _buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_launcher_foreground);// set drawable icon
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //setTitle("Nauja prgrama");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getBaseContext(), AddStore.class);
                //intent.putExtra("Data", "Hello World");
                startActivity(intent1);
            }
        });

        //mEditText = findViewById(R.id.edit_text);   //To save stuff into a file

        Draw();
    }

    private void Draw() {
        setFigure(1, true);
    }

    public void setFigure(final int figure, final boolean fillFlag){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                drawView.setFigure(figure);
                drawView.setFillFlag(fillFlag);
                drawView.invalidate();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        /*if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }*/

        Log.i("menu", "true");
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    /*  //Saving into a file and loading from it
    public void save(View v) {
        String text = mEditText.getText().toString();
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(text.getBytes());

            mEditText.getText().clear();
            Toast.makeText(this, "Saved to " + getFilesDir() + "/" + FILE_NAME,
                    Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void load(View v) {
        FileInputStream fis = null;

        try {
            fis = openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }

            mEditText.setText(sb.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    */

}