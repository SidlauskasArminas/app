package god.knows.application.Settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;
import god.knows.application.Student;

public class IndividualWork extends DrawerMain {

    ListView simpleList;
    Button buttonDisappear;
    Button buttonSort;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_individual_work, null, false);
        drawerLayout.addView(contentView, 0);

        simpleList = (ListView)findViewById(R.id.ListView_ind_work);

        buttonDisappear = (Button) findViewById(R.id.button_settings_ind_disappear);
        buttonDisappear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView = (TextView) findViewById(R.id.textView_settings_ind_disappear);
                textView.setVisibility(View.GONE);
            }
        });

        Bundle bundle = getIntent().getExtras();
        final ArrayList<Student> arraylist = bundle.getParcelableArrayList("mylist");

        buttonSort = (Button) findViewById(R.id.button_settings_ind_sort);
        buttonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String products[] = getArray(arraylist);
                Arrays.sort(products);
                Collections.sort(arraylist);
                fillList(products);
            }
        });


        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String products[] = getArray(arraylist);
                Toast.makeText(IndividualWork.this, products[position], Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), IndividualWork1.class);
                intent.putExtra("Data", products[position]);
                startActivity(intent);
            }
        });

        simpleList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(IndividualWork.this, "Long clicked", Toast.LENGTH_SHORT).show();
                arraylist.remove(pos);
                final String products[] = getArray(arraylist);
                fillList(products);
                return true;
            }
        });

        final String products[] = getArray(arraylist);
        fillList(products);
    }

    public String[] getArray(final ArrayList<Student> arraylist){
        final String products[] = new String[arraylist.size()];
        int i = 0;
        for(Student Product : arraylist) {
            products[i++] = Product.getName().trim() + " " + Product.getId();
        }
        return products;
    }

    public void fillList(String products[]){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview, R.id.textView, products);
        simpleList.setAdapter(arrayAdapter);

        /*
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(IndividualWork.this, products[position], Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), IndividualWork1.class);
                intent.putExtra("Data", products[position]);
                startActivity(intent);
            }
        });

        simpleList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(IndividualWork.this, "Long clicked", Toast.LENGTH_SHORT).show();
                int a = 0;
                int b = 0;
                final String[] products1 = new String[i-1];
                while(a < i){
                    if(a != pos)
                        products1[a++] = products[b];
                    b++;
                }
                fillList(products1);
                return true;
            }
        });*/
    }


}
