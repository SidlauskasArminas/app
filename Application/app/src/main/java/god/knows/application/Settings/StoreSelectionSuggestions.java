package god.knows.application.Settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Store;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;

public class StoreSelectionSuggestions extends DrawerMain {

    private AppDatabase mDb;
    private TextView txt_list;
    ListView simpleList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_store_selection_suggestions, null, false);
        drawerLayout.addView(contentView, 0);

        mDb = AppActivity.getDatabase();

        getStorelist();
    }

    private void getStorelist() {
        final List<Store> StoreList = mDb.storeDAO().getAllStores();
        final String stores[] = new String[StoreList.size()];
        int i = 0;
        for(Store store : StoreList) {
            stores[i++] = store.getName().trim();
        }
        simpleList = (ListView)findViewById(R.id.simpleListView_store_selection_suggestions);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview1, R.id.textView, stores);
        simpleList.setAdapter(arrayAdapter);

        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), WordSuggestions.class);
                intent.putExtra("StoreId", StoreList.get(position).getId());
                startActivity(intent);
                // TODO Auto-generated method stub*/
                Toast.makeText(StoreSelectionSuggestions.this, "Clicked on store", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
