package god.knows.application.Settings;

import androidx.annotation.NonNull;

import android.content.Context;
import android.os.Bundle;
import android.os.FileUtils;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import android.provider.Settings;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.database.DatabaseReference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Product;
import god.knows.application.Database.Store;
import god.knows.application.Database.Suggestions;
import god.knows.application.Drawer.DrawView;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;

public class FirebaseBackup extends DrawerMain {

    FirebaseFirestore dbFirebase;
    private AppDatabase mDb;
    Button buttonUpload;
    Button buttonDownload;
    String androidId;
    DrawView drawView;
    //DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_firebase_backup, null, false);
        drawerLayout.addView(contentView, 0);

        drawView = (DrawView) findViewById(R.id.drawView2);

        mDb = AppActivity.getDatabase();
        dbFirebase = FirebaseFirestore.getInstance();
        //databaseReference = dbFirebase
        androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        buttonUpload = (Button) findViewById(R.id.button_upload_backup);
        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadToFireBase();
            }
        });

        buttonDownload = (Button) findViewById(R.id.button_download_backup);
        buttonDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFromFireBase();
            }
        });
    }

    private void Draw(int a) {
        setFigure(a, true);
    }

    public void setFigure(final int figure, final boolean fillFlag){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                drawView.setFigure(figure);
                drawView.setFillFlag(fillFlag);
                drawView.invalidate();
            }
        });
    }
    private void downloadFromFireBase() {
        Draw(3);
        //mDb.storeDAO().deleteAll();
        //mDb.productDAO().deleteAll();
        Draw(4);
        Draw(5);
        dbFirebase.collection(androidId).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Draw(5);
                            List<DocumentSnapshot> myListOfDocuments = task.getResult().getDocuments();
                            Draw(6);
                            for(int i = 0; i < myListOfDocuments.size(); i++) {
                                Map<String, Object> singleDoc = myListOfDocuments.get(i).getData();
                                long amount = (long) singleDoc.get("productAmount");
                                String storeName = (String) singleDoc.get("storeName");

                                Date date1 = new Date();
                                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if(mDb.storeDAO().checkIfExists(storeName) == 0){
                                    Store store = new Store();
                                    store.setName(storeName);
                                    store.setDate(dateFormat.format(date1));
                                    mDb.storeDAO().insert(store);
                                }

                                int storeId = mDb.storeDAO().getStoreIdByName(storeName);
                                String[] products = new String[(int)amount];

                                for(int j = 0; j < amount; j++){
                                    Product product = new Product();
                                    product.setStoreId(storeId);
                                    product.setDate(dateFormat.format(date1));
                                    product.setDescription((String) singleDoc.get("description"+j));
                                    mDb.productDAO().insert(product);
                                    Suggestions suggestions = new Suggestions();
                                    suggestions.setWord(product.getDescription());
                                    suggestions.setStoreId(storeId);
                                    if(mDb.suggestionsDAO().checkIfExists(suggestions.getWord())==0)
                                        mDb.suggestionsDAO().insert(suggestions);
                                }

                            }
                            //StringBuilder amount = new StringBuilder("");
                            //String amount = myListOfDocuments.get(0).getString("productAmount");
                            //String key = myListOfDocuments.get(0).getString("key");
                            //key.replace(androidId, "");
                            Draw(7);
                            Toast.makeText(FirebaseBackup.this, "Backup downloaded and placed", Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }

    private void uploadToFireBase() {
        Draw(3);
        final List<Store> StoreList = mDb.storeDAO().getAllStores();
        for(int i = 0; i < StoreList.size(); i++)
        {
            if(i == 1)
                Draw(4);
            StoreList.get(i).getName();
            final List<Product> ProductList = mDb.productDAO().getAllProducts(StoreList.get(i).getId());
            Map<String, Object> newContact = new HashMap<>();
            newContact.put("storeName", StoreList.get(i).getName());
            for(int j = 0; j < ProductList.size(); j++)
            {
                newContact.put("description"+j, ProductList.get(j).getDescription());
            }
            newContact.put("productAmount", ProductList.size());
            dbFirebase.collection(androidId).document(StoreList.get(i).getName())
                    .set(newContact)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }

                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("ERROR", "Pieva neikele");
                        }
                    });
        }
        Draw(7);
        Toast.makeText(FirebaseBackup.this, "Added new to backup", Toast.LENGTH_SHORT).show();
    }

}
