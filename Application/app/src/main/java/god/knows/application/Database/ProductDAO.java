package god.knows.application.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import god.knows.application.Database.Product;

@Dao
public interface ProductDAO {
    @Insert
    void insert(Product product);

    @Query("DELETE FROM Product")
    void deleteAll();

    @Query("DELETE FROM Product WHERE id = :number")
    void delete(long number);

    @Query("SELECT * from Product WHERE StoreId = :SId  ORDER BY description ASC")
    List<Product> getAllProducts(long SId);
}
