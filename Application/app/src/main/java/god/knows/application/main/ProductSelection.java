package god.knows.application.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Product;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.MySimpleArrayAdapter;
import god.knows.application.R;

public class ProductSelection extends DrawerMain {

    private AppDatabase mDb;
    private TextView txt_list;
    ListView simpleList;
    String countryList[] = {"India", "China", "australia", "Portugal", "America", "NewZealand"};
    String removedVal;
    Context contextFoodSelection;
    Dialog myDialog;
    Button buttonTime;
    Button buttonLoc;
    Button removeChecked;
    long storeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_product_selection, null, false);
        drawerLayout.addView(contentView, 0);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_launcher_foreground);// set drawable icon
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*Button myButton = new Button(this);
        myButton.setText("<");

        LinearLayout ll = (LinearLayout)findViewById(R.id.linear2);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ll.addView(myButton, lp);*/

        storeId = getIntent().getLongExtra("StoreId", 0);

        FloatingActionButton fab = findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getBaseContext(), AddProduct.class);
                intent1.putExtra("StoreId", storeId);
                startActivity(intent1);
            }
        });

        contextFoodSelection = this;
        myDialog = new Dialog(this);
        mDb = AppActivity.getDatabase();
        txt_list = (TextView) findViewById(R.id.textView);
        /*simpleList = (ListView)findViewById(R.id.simpleListView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview, R.id.textView, countryList);
        simpleList.setAdapter(arrayAdapter);*/
        getFoodList(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(ProductSelection.this, "Pavyko", Toast.LENGTH_SHORT).show();
                ShowPopup();
                return true;
            }
        });
        return true;
    }

    private void getFoodList1() {
        List<Product> ProductList = mDb.productDAO().getAllProducts(storeId);
        String products[] = new String[ProductList.size()];
        int i = 0;
        for(Product Product : ProductList) {
            products[i++] = Product.getDescription().trim();
        }
    }

    private void getFoodList(boolean removed) {
        if(removed)
            Toast.makeText(ProductSelection.this, removedVal + " removed from list.", Toast.LENGTH_SHORT).show();
        final List<Product> ProductList = mDb.productDAO().getAllProducts(storeId);
        final String products[] = new String[ProductList.size()];
        int i = 0;
        for(Product Product : ProductList) {
            products[i++] = Product.getDescription().trim();
        }
        simpleList = (ListView)findViewById(R.id.simpleListView);
        final MySimpleArrayAdapter arrayAdapter = new MySimpleArrayAdapter(this,ProductList);
        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview, R.id.textView, products);
        simpleList.setAdapter(arrayAdapter);

        removeChecked = (Button) findViewById(R.id.button_remove_checked);
        removeChecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean[] checked = arrayAdapter.getChecked();
                for (int i = 0; i < checked.length; i++){
                    if(checked[i])
                        mDb.productDAO().delete(ProductList.get(i).getId());
                }
                if(checked.length == 0)
                    Toast.makeText(ProductSelection.this, "There is nothing to remove!", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(ProductSelection.this, "Removed selected", Toast.LENGTH_SHORT).show();
                getFoodList(false);
            }
        });
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                removedVal = ProductList.get(position).getDescription();
                mDb.productDAO().delete(ProductList.get(position).getId());
                getFoodList(true);
                // TODO Auto-generated method stub
                Toast.makeText(ProductSelection.this, ProductList.get(position).getId()+ "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ShowPopup(){
        TextView txtclose;
        myDialog.setContentView(R.layout.notification_popup);
        txtclose = (TextView) myDialog.findViewById(R.id.textView_close_popup);
        buttonTime = (Button) myDialog.findViewById(R.id.button_select_time_notification);
        buttonLoc = (Button) myDialog.findViewById(R.id.button_select_location_notification);

        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        buttonLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLoc.setBackgroundResource(R.drawable.roundbuttonstyleclick);
                myDialog.dismiss();
            }
        });

        buttonTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonTime.setBackgroundResource(R.drawable.roundbuttonstyleclick);
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
        myDialog.show();
    }

    public void checkOutAllItems(){
        final List<Product> ProductList = mDb.productDAO().getAllProducts(storeId);
        final String products[] = new String[ProductList.size()];


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, StoreSelection.class);
        startActivity(intent);
        super.onBackPressed();
    }
}

