package god.knows.application.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Store;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;

public class AddStore extends DrawerMain {

    private AppDatabase mDb;
    private Button button;
    private EditText etDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_add_store, null, false);
        drawerLayout.addView(contentView, 0);

        mDb = AppActivity.getDatabase();
        etDescription = (EditText) findViewById(R.id.auto_complete_textview_add_store);

        button = (Button) findViewById(R.id.button_add_store);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = etDescription.getText().toString().trim();

                if(TextUtils.isEmpty(description)) {
                    Toast.makeText(getApplicationContext(),
                            "Store should not be empty",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if(mDb.storeDAO().checkIfExists(description)>0)
                    {
                        Toast.makeText(getApplicationContext(),
                                "There exists such a store already",
                                Toast.LENGTH_SHORT).show();
                    }else {
                        Store store = new Store();
                        store.setName(description);

                        Date date1 = new Date();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        store.setDate(dateFormat.format(date1));
                        mDb.storeDAO().insert(store);
                        Toast.makeText(getApplicationContext(), "Saved store successfully", Toast.LENGTH_SHORT).show();
                        etDescription.setText("");
                        etDescription.requestFocus();
                        finish();
                        startActivity(getIntent());
                        //getSuggestionList();
                    }
                }
            }
        });
        //getSuggestionList();
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_save, 0, 0, 0);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, StoreSelection.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
