package god.knows.application.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import god.knows.application.Database.Suggestions;

@Dao
public interface SuggestionsDAO {
    @Insert
    void insert(Suggestions suggestions);

    @Query("DELETE FROM Suggestions WHERE StoreId = :storeId")
    void deleteAll(long storeId);

    @Query("DELETE FROM Suggestions WHERE id = :number")
    void delete(long number);

    @Query("SELECT * from Suggestions WHERE StoreId = :SId ORDER BY word ASC")
    List<Suggestions> getAllSuggestions(long SId);

    @Query("SELECT * from Suggestions ORDER BY word ASC")
    List<Suggestions> getAllSuggestions();

    @Query("SELECT COUNT(*) from Suggestions WHERE word = :word")
    int checkIfExists(String word);
}

