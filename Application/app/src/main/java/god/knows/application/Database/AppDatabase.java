package god.knows.application.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;



@Database(version = 8, entities = {Product.class, Suggestions.class, Store.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract ProductDAO productDAO();
    public abstract SuggestionsDAO suggestionsDAO();
    public abstract StoreDAO storeDAO();
}
