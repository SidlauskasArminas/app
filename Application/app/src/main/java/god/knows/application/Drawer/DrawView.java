package god.knows.application.Drawer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.Collections;

public class DrawView extends View {
    private static final int NONE = 0;
    private static final int TRIANGLE = 1;
    private static final int CIRCLE = 2;
    private static final int RECTANGLE = 3;

    public boolean fillFlag = false;

    private int figure;

    public DrawView(Context context) {
        super(context);
    }

    public DrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int getFigure(){
        return figure;
    }

    public void setFigure(int figure) {
        this.figure = figure;
    }

    public void setFillFlag(int figure){
        this.figure = figure;
    }

    public boolean getFillFlag(){
        return fillFlag;
    }

    public void setFillFlag(boolean fillFlag) {
        this.fillFlag = fillFlag;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        Paint paint;

        switch (figure){
            case TRIANGLE:{
                paint = new Paint();
                paint.setColor(Color.WHITE);

                if(fillFlag) {paint.setStyle(Paint.Style.FILL_AND_STROKE);}
                else { paint.setStyle(Paint.Style.STROKE);}

                paint.setStrokeWidth(1f);

                Point point1_draw = new Point(width/2, 0);
                Point point2_draw = new Point(0, height/2);
                Point point3_draw = new Point(width, height/2);

                Path path = new Path();
                path.moveTo(point1_draw.x, point1_draw.y);
                path.lineTo(point2_draw.x, point2_draw.y);
                path.lineTo(point3_draw.x, point3_draw.y);
                path.lineTo(point1_draw.x, point1_draw.y);
                path.close();
                canvas.drawPath(path, paint);
                paint.setColor(Color.BLACK);
                final float testTextSize = 20f;
                paint.setTextSize(testTextSize);
                canvas.drawText("HOME", width/10*4, height/3, paint);
                paint.setColor(Color.WHITE);
                canvas.drawRect(40,height/2, 110, height, paint);
                canvas.drawRect(170,height/2, 240, height, paint);
                canvas.drawRect(50,height/2, 170, 120, paint);
                //canvas.drawRect(width/8,height/2,width/8*2, height/2, paint);
                break;
            }
            case CIRCLE:{
                paint = new Paint();
                paint.setColor(Color.BLUE);
                if(fillFlag){
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                }
                else{
                    paint.setStyle(Paint.Style.STROKE);
                }
                paint.setStrokeWidth(10f);

                canvas.drawCircle(width/2, height/2, width/2, paint);
                break;
            }
            case RECTANGLE:{
                paint = new Paint();
                paint.setColor(Color.rgb(225, 250, 229));
                if(fillFlag){
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                }
                else {
                    paint.setStyle(Paint.Style.STROKE);
                }
                paint.setStrokeWidth(10f);
                canvas.drawRect(0,0, 120, height, paint);
                break;
            }
            case 4:{
                paint = new Paint();
                if(fillFlag){paint.setStyle(Paint.Style.FILL_AND_STROKE);}
                else {paint.setStyle(Paint.Style.STROKE);}
                paint.setStrokeWidth(10f);
                paint.setColor(Color.rgb(225, 250, 229));
                canvas.drawRect(0,0, 120, height, paint);
                paint.setColor(Color.rgb(188,255,199));
                canvas.drawRect(120,0, 240, height, paint);
                break;
            }
            case 5:{
                paint = new Paint();
                if(fillFlag){paint.setStyle(Paint.Style.FILL_AND_STROKE);}
                else {paint.setStyle(Paint.Style.STROKE);}
                paint.setStrokeWidth(10f);
                paint.setColor(Color.rgb(225, 250, 229));
                canvas.drawRect(0,0, 120, height, paint);
                paint.setColor(Color.rgb(188,255,199));
                canvas.drawRect(120,0, 240, height, paint);
                paint.setColor(Color.rgb(142,255,161));
                canvas.drawRect(240,0, 360, height, paint);
                break;
            }
            case 6:{
                paint = new Paint();
                if(fillFlag){paint.setStyle(Paint.Style.FILL_AND_STROKE);}
                else {paint.setStyle(Paint.Style.STROKE);}
                paint.setStrokeWidth(10f);
                paint.setColor(Color.rgb(225, 250, 229));
                canvas.drawRect(0,0, 120, height, paint);
                paint.setColor(Color.rgb(188,255,199));
                canvas.drawRect(120,0, 240, height, paint);
                paint.setColor(Color.rgb(142,255,161));
                canvas.drawRect(240,0, 360, height, paint);
                paint.setColor(Color.rgb(74, 250, 104));
                canvas.drawRect(360,0, 480, height, paint);
                break;
            }
            case 7:{
                paint = new Paint();
                if(fillFlag){paint.setStyle(Paint.Style.FILL_AND_STROKE);}
                else {paint.setStyle(Paint.Style.STROKE);}
                paint.setStrokeWidth(10f);
                paint.setColor(Color.rgb(225, 250, 229));
                canvas.drawRect(0,0, 120, height, paint);
                paint.setColor(Color.rgb(188,255,199));
                canvas.drawRect(120,0, 240, height, paint);
                paint.setColor(Color.rgb(142,255,161));
                canvas.drawRect(240,0, 360, height, paint);
                paint.setColor(Color.rgb(74, 250, 104));
                canvas.drawRect(360,0, 480, height, paint);
                paint.setColor(Color.rgb(0, 250, 41));
                canvas.drawRect(480,0, 600, height, paint);
                break;
            }
            case NONE:{

            }
            default: break;
        }

    }
}

