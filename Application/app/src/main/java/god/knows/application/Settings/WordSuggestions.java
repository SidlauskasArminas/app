package god.knows.application.Settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Suggestions;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;

public class WordSuggestions extends DrawerMain {

    private AppDatabase mDb;
    private TextView txt_list;
    ListView simpleList;
    private String removedVal;
    long storeId;
    Button button_remove_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_word_suggestions, null, false);
        drawerLayout.addView(contentView, 0);


        mDb = AppActivity.getDatabase();
        txt_list = (TextView) findViewById(R.id.textViewWordSuggestions);
        storeId = getIntent().getLongExtra("StoreId", 0);
        getSuggestionsList(false);

        final Context context = this;
        button_remove_all = (Button) findViewById(R.id.button_remove_all_word_suggestions);
        button_remove_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setTitle("Remove all suggestions")
                        .setMessage("Are you sure you wish to remove all saved suggestions?")
                        .setPositiveButton("Yes", null)
                        .setNegativeButton("No", null)
                        .show();

                Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(WordSuggestions.this, "Removed all saved suggestions", Toast.LENGTH_SHORT).show();
                        mDb.suggestionsDAO().deleteAll(storeId);
                        dialog.dismiss();
                        getSuggestionsList(false);
                    }
                });
            }
        });

    }

    private void getSuggestionsList(boolean removed) {
        if(removed)
            Toast.makeText(WordSuggestions.this, removedVal + " removed from list.", Toast.LENGTH_SHORT).show();
        final List<Suggestions> SuggestionsList = mDb.suggestionsDAO().getAllSuggestions(storeId);
        final String suggestions[] = new String[SuggestionsList.size()];
        int i = 0;
        for(Suggestions Suggestion : SuggestionsList) {
            suggestions[i++] = Suggestion.getWord().trim();
        }
        simpleList = (ListView)findViewById(R.id.ListViewForWordSuggestions);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview1, R.id.textView, suggestions);
        simpleList.setAdapter(arrayAdapter);

        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                removedVal = SuggestionsList.get(position).getWord();
                mDb.suggestionsDAO().delete(SuggestionsList.get(position).getId());
                getSuggestionsList(true);
                // TODO Auto-generated method stub
                Toast.makeText(WordSuggestions.this, SuggestionsList.get(position).getId()+ "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, StoreSelectionSuggestions.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
