package god.knows.application.Database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Store {
    @NonNull
    @PrimaryKey(autoGenerate =  true)
    @ColumnInfo(name = "id")
    private long id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "date")
    private String date;

    @NonNull
    public void setId(@NonNull long id){
        this.id = id;
    }

    @NonNull
    public long getId(){ return this.id; }

    @NonNull
    public void setName(@NonNull String name) { this.name = name; }

    @NonNull
    public String getName() { return name; }

    @NonNull
    public void setDate(@NonNull String date) {
        this.date = date;
    }

    @NonNull
    public String getDate() { return this.date;}
}
