package god.knows.application.Settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import god.knows.application.Database.AppDatabase;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;
import god.knows.application.Student;

public class Settings extends DrawerMain {

    private AppDatabase mDb;
    Button buttonWordSuggestions;
    Button buttonIndWork;
    Button buttonFirebase;
    private Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_settings, null, false);
        drawerLayout.addView(contentView, 0);

        buttonWordSuggestions = (Button) findViewById(R.id.button_remove_word_suggestions);
        buttonWordSuggestions.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //onClick
                Intent intent = new Intent(getBaseContext(), StoreSelectionSuggestions.class);
                //intent.putExtra("Data", "Hello World");
                startActivity(intent);
            }
        });

        final ArrayList<Student> arraylist = new ArrayList<Student>();
        Student a = new Student(1, "Petras");
        Student b = new Student(2, "Jonas");
        arraylist.add(a);
        arraylist.add(b);
        b = new Student(100, "Arminas");
        arraylist.add(b);
        buttonIndWork = (Button) findViewById(R.id.button_settings_ind);
        buttonIndWork.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //onClick
                Intent intent = new Intent(getBaseContext(), IndividualWork.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("mylist", arraylist);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        buttonFirebase = (Button) findViewById(R.id.button_firebase);
        buttonFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), FirebaseBackup.class);
                //intent.putExtra("Data", "Hello World");
                startActivity(intent);
            }
        });
    }
}
