package god.knows.application.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import god.knows.application.Database.AppActivity;
import god.knows.application.Database.AppDatabase;
import god.knows.application.Database.Store;
import god.knows.application.Drawer.DrawerMain;
import god.knows.application.R;

public class StoreSelection extends DrawerMain {


    private AppDatabase mDb;
    private TextView txt_list;
    ListView simpleList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_store_selection, null, false);
        drawerLayout.addView(contentView, 0);

        mDb = AppActivity.getDatabase();

        FloatingActionButton fab = findViewById(R.id.fab_store_selection);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getBaseContext(), AddStore.class);
                //intent.putExtra("Data", "Hello World");
                startActivity(intent1);
            }
        });

        getStorelist();
    }

    private void getStorelist() {
        final List<Store> StoreList = mDb.storeDAO().getAllStores();
        final String stores[] = new String[StoreList.size()];
        int i = 0;
        for(Store store : StoreList) {
            stores[i++] = store.getName().trim();
        }
        simpleList = (ListView)findViewById(R.id.simpleListView_store_selection);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview1, R.id.textView, stores);
        simpleList.setAdapter(arrayAdapter);
        //simpleList.setItemsCanFocus(true);
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), ProductSelection.class);
                intent.putExtra("StoreId", StoreList.get(position).getId());
                startActivity(intent);
                // TODO Auto-generated method stub*/
                Toast.makeText(StoreSelection.this, "Clicked on store", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
