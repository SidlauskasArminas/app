package god.knows.application.Database;


import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class Product {
    @NonNull
    @PrimaryKey(autoGenerate =  true)
    @ColumnInfo(name = "id")
    private long id;

    @NonNull
    @ColumnInfo(name = "description")
    private String description;

    @NonNull
    @ColumnInfo(name = "date")
    private String date;

    @NonNull
    @ColumnInfo(name = "StoreId")
    private long StoreId;


    @NonNull
    public void setId(@NonNull long id){
        this.id = id;
    }

    @NonNull
    public long getId(){ return this.id; }

    @NonNull
    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getDescription(){
        return this.description;
    }

    @NonNull
    public void setDate(@NonNull String date) {
        this.date = date;
    }

    @NonNull
    public String getDate() { return this.date;}

    @NonNull
    public void setStoreId(@NonNull long storeId) { StoreId = storeId; }

    @NonNull
    public long getStoreId() { return StoreId; }


}
