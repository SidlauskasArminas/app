package god.knows.application;

import android.app.LauncherActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import java.util.List;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import god.knows.application.Database.Product;
import android.widget.TextView;
import android.widget.CheckBox;

import god.knows.application.Database.Product;

public class MySimpleArrayAdapter extends ArrayAdapter<Product>
{
    private boolean[] checked;

    public MySimpleArrayAdapter(Context context, List<Product> objects){
        super(context, R.layout.activity_listview, objects);
        checked = new boolean[objects.size()];
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.activity_listview, null);
        }
        TextView text = (TextView) v.findViewById(R.id.textView);
        CheckBox check = (CheckBox) v.findViewById(R.id.checkBox);

        Product product = getItem(position);

        text.setText(product.getDescription());
        check.setChecked(checked[position]);

        check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setChecked(position);
            }
        });
        return v;
    }

    public void setChecked(int a) {
        if(checked[a])
            checked[a] = false;
        else
            checked[a] = true;
    }
    public boolean[] getChecked(){
        return checked;
    }

    /*@Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listitem, parent, false);
        CheckBox checkBox = (CheckBox)rowView.findViewById(R.id.checkBox);
        TextView newwordview = (TextView)rowView.findViewById(R.id.newwordview);

        Item item = getItem(position);
        newwordview.setText(item.title);
        checkBox.setChecked(item.checked);


        checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked)
            {
                Item item = getItem(position);
                item.checked = isChecked;
            }
        });

        return rowView;
    }*/
}
